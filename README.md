# Module Boilerplate

This is my preferred boilerplate for starting a new node module. It has:

- Babel fully configured to compile down to commonjs and es modules (to enable tree shaking)
- Linting based on the airbnb configs (with some of my preferred overwrites)
- Jest fully configured for testing with React


## Compiling

If you want to compile as commonjs:

```
npm run build:cjs
```

To compile as es modules:

```
npm run build:esm
```

To build as both:

```
npm run build
```

## Testing

To run tests only (no linting)

```
npm run test:run
```

To run tests on every code change:

```
npm run test:watch
```

To run tests with linting

```
npm test
```

## Linting

To run lint test

```
npm run lint
```
