module.exports = {
  "extends": "airbnb",
  "parser": "babel-eslint",
  "plugins": ["react", "jsx", "jsx-a11y", "import"],
  "env": {
    "browser": true,
    "es6": true
  },
  "rules": {
    "brace-style": [2, "stroustrup", {
      "allowSingleLine": true
    }],
    "import/no-named-as-default": [0],
    "no-underscore-dangle": [0],
    "react/jsx-closing-tag-location": [0],
    "react/jsx-filename-extension": [2, {
      "extensions": [".js"]
    }],
  },
  "globals": {
    "afterEach": true,
    "beforeEach": true,
    "describe": true,
    "expect": true,
    "jest": true,
    "it": true,
  },
};
