import React from 'react';
import PropTypes from 'prop-types';

const SomeComponent = props => (
  <div>{ props.someVar }</div>
);

SomeComponent.defaultProps = {
  someVar: 'A Variable',
};

SomeComponent.propTypes = {
  someVar: PropTypes.string,
};

export default SomeComponent;
