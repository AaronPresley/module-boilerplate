import React from 'react';
import renderer from 'react-test-renderer';

import SomeComponent from '../src/some-component';


describe('SomeComponent', () => {
  it('should have the expected default prop', () => {
    const component = renderer.create(
      <SomeComponent />
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should accept a passed prop', () => {
    const component = renderer.create(
      <SomeComponent someVar="New Var Here" />
    );

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
